package de.dieunkreativen.redalert;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Random;

import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

public class CustomEntityFirework {
	
    public CustomEntityFirework(FireworkEffect fe, Location loc) {
        Firework f = (Firework) loc.getWorld().spawn(loc, Firework.class);
        FireworkMeta fm = f.getFireworkMeta();
        fm.addEffect(fe);
        f.setFireworkMeta(fm);
        try {
            Class<?> entityFireworkClass = Reflection.getNMSClass("EntityFireworks");
            Class<?> craftFireworkClass = Reflection.getOBCClass("entity.CraftFirework");
            Object firework = craftFireworkClass.cast(f);
            Method handle = firework.getClass().getMethod("getHandle");
            Object entityFirework = handle.invoke(firework);
            Field expectedLifespan = entityFireworkClass.getDeclaredField("expectedLifespan");
            Field ticksFlown = entityFireworkClass.getDeclaredField("ticksFlown");
            ticksFlown.setAccessible(true);
            ticksFlown.setInt(entityFirework, expectedLifespan.getInt(entityFirework) - 1);
            ticksFlown.setAccessible(false);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

	public static void spawn(Location location, FireworkEffect effect) {
	    new CustomEntityFirework(effect, location);
	}
	
	public static FireworkEffect getRandomEffect() {
		Color color [] = {
				Color.WHITE,
				Color.RED,
				Color.AQUA,
				Color.GREEN,
				Color.BLUE,
				Color.YELLOW
		};
		
		Type type [] = {
				Type.BALL,
				Type.BALL_LARGE,
				Type.BURST,
				Type.CREEPER,
				Type.STAR
		};
		
		return FireworkEffect.builder().with(type[new Random().nextInt(type.length)]).withTrail().withColor(color[new Random().nextInt(color.length)]).build();
	}
}

