/*
* RedAlert is a minigame plugin for bukkit.
* Copyright (C) Paaattiii <http://www.dieunkreativen.de>
*
* This program is free software: you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by the
* Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT
* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
* FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
* for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*/

package de.dieunkreativen.redalert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

import mc.alk.arena.objects.ArenaPlayer;
import mc.alk.arena.objects.arenas.Arena;
import mc.alk.arena.objects.events.ArenaEventHandler;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import com.sk89q.worldguard.protection.managers.RegionManager;

public class RedAlertArena extends Arena {
	Map<Location, Material> locations = new HashMap<Location, Material>();
	int task1, task2, campTimer, fireworkDelayTask, randomBlockTimer;
	int minX, minZ, minY;
	int maxX, maxZ, maxY;
	ArrayList<String> deadplayers = new ArrayList<String>();
	boolean timer;
  
	public void onOpen() {
		timer = false;
	}
  
	public void onStart() {
		timer = true;
		antiCamp();
		randomBlockSelect();
		loadArena(getMatch().getArena().getName(), Bukkit.getWorld(getMatch().getArena().getWorldGuardRegion().getWorldName()));
	}
  
	public void onFinish() {
		Bukkit.getScheduler().cancelTask(task1);
		Bukkit.getScheduler().cancelTask(campTimer);
		Bukkit.getScheduler().cancelTask(randomBlockTimer);
		deadplayers.clear();
		task2 = Bukkit.getScheduler().scheduleSyncDelayedTask(RedAlert.getSelf(), new Runnable() {
			public void run() {
				regenLayers();
				locations.clear();
				Bukkit.getScheduler().cancelTask(task2);
			}
		}, 20L);
	}
  
  
	private void randomBlockSelect() {
		randomBlockTimer = Bukkit.getScheduler().scheduleSyncRepeatingTask(RedAlert.getSelf(), new Runnable(){
			public void run() {
				if(!locations.isEmpty()) {
					Random       random    = new Random();
					List<Location> keys      = new ArrayList<Location>(locations.keySet());
					Location       loc = keys.get(random.nextInt(keys.size()) );
					blockChange(loc);
				}
			}
		}, Defaults.speed, 0);
	}
	
	private void antiCamp() {
		campTimer = Bukkit.getScheduler().scheduleSyncRepeatingTask(RedAlert.getSelf(), new Runnable(){
			public void run() {
				Collection<ArenaPlayer> players = getMatch().getAlivePlayers();
				for (ArenaPlayer ap: players){
					Location ploc = ap.getPlayer().getLocation();
					ploc.setY(ploc.getY() - 1);
					Location l = getPlayerStandOnBlockLocation(ploc);
					blockSelect(l);
					ap.getPlayer().setFoodLevel(20);
				}
			}
		}, 10, 10);
	}

	private Location getPlayerStandOnBlockLocation(Location locationUnderPlayer) {
		Location b11 = locationUnderPlayer.clone().add(0.3,0,-0.3);
		if (b11.getBlock().getType() != Material.AIR && b11.getBlock().getType() == Material.STAINED_CLAY) {
			return b11;
		}
		Location b12 = locationUnderPlayer.clone().add(-0.3,0,-0.3);
		if (b12.getBlock().getType() != Material.AIR && b12.getBlock().getType() == Material.STAINED_CLAY) {
			return b12;
		}
		Location b21 = locationUnderPlayer.clone().add(0.3,0,0.3);
		if (b21.getBlock().getType() != Material.AIR && b21.getBlock().getType() == Material.STAINED_CLAY) {
			return b21;
		}
		Location b22 = locationUnderPlayer.clone().add(-0.3,0,+0.3);
		if (b22.getBlock().getType() != Material.AIR && b22.getBlock().getType() == Material.STAINED_CLAY) {
			return b22;
		}
		return locationUnderPlayer;
	}
  

	@ArenaEventHandler(suppressCastWarnings=true)
	public void onPlayerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		Location loc = event.getPlayer().getLocation();
		Location newloc = new Location(loc.getWorld(), loc.getX(), loc.getY() - 1.0, loc.getZ());

		if (newloc.getBlock().getType() == Material.WATER) {
			if(!deadplayers.contains(player.getName())) {
				PlayerDeathEvent ede = new PlayerDeathEvent(player, new ArrayList<ItemStack>(), 0, "");
				Bukkit.getPluginManager().callEvent(ede);
				deadplayers.add(player.getName());
			}
		}
	} 
  
	public void blockSelect(final Location loc) {
		if (timer) {
			task1 = Bukkit.getScheduler().scheduleSyncDelayedTask(RedAlert.getSelf(), new Runnable() {
				public void run() {
					blockChange(loc);
				}
	        }, Defaults.delay);
		}
	}
  
	@ArenaEventHandler
	public void onEntityDamage(EntityDamageEvent event) {
		if ((event.getEntity() instanceof Player)) {
			final Player player = (Player)event.getEntity();
			if (event.getCause().equals(EntityDamageEvent.DamageCause.FALL)) {
				event.setCancelled(true);
			}
			if ((event.getCause().equals(EntityDamageEvent.DamageCause.VOID)) || (event.getCause().equals(EntityDamageEvent.DamageCause.LAVA))) {
				if(!deadplayers.contains(player.getName())) {
					event.setCancelled(true);
					PlayerDeathEvent ede = new PlayerDeathEvent(player, new ArrayList<ItemStack>(), 0, "");
		            Bukkit.getPluginManager().callEvent(ede);
					deadplayers.add(player.getName());
					
					Bukkit.getScheduler().scheduleSyncDelayedTask(RedAlert.getSelf(), new Runnable() {
				        public void run() {
				        	player.setFireTicks(0);
				        }
					}, 20L);
				}
			}
		}
	}
  
	@SuppressWarnings("deprecation")
	public void regenLayers() {
		loadArena(getMatch().getArena().getName(), Bukkit.getWorld(getMatch().getArena().getWorldGuardRegion().getWorldName()));
		for (Map.Entry<Location, Material> e : locations.entrySet())
		{
			Location loc = (Location)e.getKey();
			loc.getBlock().setType(Material.STAINED_CLAY);
			loc.getBlock().setData(DyeColor.WHITE.getData());
		}
	}
	
	@SuppressWarnings("deprecation")
	public void blockChange(Location loc) {
		
		if(loc.getBlock().getData() == DyeColor.WHITE.getData() && loc.getBlock().getType() == Material.STAINED_CLAY && loc.getBlock().getType() != Material.AIR) {
			loc.getBlock().setType(Material.STAINED_CLAY);
			loc.getBlock().setData(DyeColor.YELLOW.getData());
			locations.put(loc.getBlock().getLocation(), loc.getBlock().getType());
		}
		else if(loc.getBlock().getData() == DyeColor.YELLOW.getData() && loc.getBlock().getType() == Material.STAINED_CLAY && loc.getBlock().getType() != Material.AIR) {
			loc.getBlock().setType(Material.STAINED_CLAY);
			loc.getBlock().setData(DyeColor.ORANGE.getData());
			locations.put(loc.getBlock().getLocation(), loc.getBlock().getType());
		}
		else if(loc.getBlock().getData() == DyeColor.ORANGE.getData() && loc.getBlock().getType() == Material.STAINED_CLAY && loc.getBlock().getType() != Material.AIR) {
			loc.getBlock().setType(Material.STAINED_CLAY);
			loc.getBlock().setData(DyeColor.RED.getData());
			locations.put(loc.getBlock().getLocation(), loc.getBlock().getType());
		}
		else if(loc.getBlock().getData() == DyeColor.RED.getData() && loc.getBlock().getType() == Material.STAINED_CLAY && loc.getBlock().getType() != Material.AIR) {
			loc.getBlock().setType(Material.AIR);
			playFirework(loc);
			locations.remove(loc.getBlock().getLocation());
		}
		
	}
	
    public void loadArena(String arena, World world){
    	RegionManager rm = RedAlert.wp.getRegionManager(world);
    	
    	minX = rm.getRegion("ba-" + arena).getMinimumPoint().getBlockX();
    	minZ = rm.getRegion("ba-" + arena).getMinimumPoint().getBlockZ();
		minY = rm.getRegion("ba-" + arena).getMinimumPoint().getBlockY();
		
		maxX = rm.getRegion("ba-" + arena).getMaximumPoint().getBlockX();
		maxZ = rm.getRegion("ba-" + arena).getMaximumPoint().getBlockZ();
		maxY = rm.getRegion("ba-" + arena).getMaximumPoint().getBlockY();
		
		for (int x = minX; x <= maxX; ++x) {
			for (int y = minY; y <= maxY; ++y) {
				for (int z = minZ; z <= maxZ; ++z) {
					Location loc = new Location(world, x, y, z);
					locations.put(loc.getBlock().getLocation(), loc.getBlock().getType());
				}
			}
		}
   }
    
	public void playFirework(final Location loc) {
		task1 = Bukkit.getScheduler().scheduleSyncDelayedTask(RedAlert.getSelf(), new Runnable() {
            public void run() {
        		CustomEntityFirework.spawn(loc, FireworkEffect.builder().with(Type.BALL).withColor(Color.RED).build());
        		Bukkit.getScheduler().cancelTask(task1);
            }
		}, 1L); 
	}
	
  
	public Collection<ArenaPlayer> getArenaPlayer() {
		return getMatch().getAlivePlayers();
	}
}
